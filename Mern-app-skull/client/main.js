//entry point to render the complete React app
//imported the root or top-level React component
//that will contain the whole frontend 
//and render it to the div element with 
//the 'root' ID specified in the HTML document in template.js.
//top level component app.js it's been rendered in the HTML
import React from 'react'
import { hydrate } from 'react-dom'
import App from './App'

hydrate(<App/>, document.getElementById('root'))
