//using sessionStorage over localStorage to
//store the JWT credentials.
//sessionStorage user auth state will only be
//remembered in the current window tab.
//localStorage user auth state will be
//remembered across tabs in a browser.


import { signout } from './api-auth.js'

const auth = {
  //retrieving credentials
  //if the current user is signed in
  //isAuthenticated() we retrieve credentials
  //from sessionStorage
  isAuthenticated() {
    if (typeof window == "undefined") return false;

    if (sessionStorage.getItem("jwt"))
      return JSON.parse(sessionStorage.getItem("jwt"));
    else return false;
  },
  //successful sign-in
  authenticate(jwt, cb) {
    if (typeof window !== "undefined")
      sessionStorage.setItem("jwt", JSON.stringify(jwt));
    cb();
  },
  //user successfully signout from app
  //clear the stored JWT CREDENTIALS
  //from sessionStorage
  clearJWT(cb) {
    if (typeof window !== "undefined") sessionStorage.removeItem("jwt");
    cb();
    //optional
    signout().then((data) => {
      document.cookie = "t=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    });
  },
};

export default auth
