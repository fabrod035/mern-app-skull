//auth-related API endpoints for sign-in and sign-out
//mounted on the express app server/express.js
//auth APIs defined in the auth.routes.js file using express.Router() line 7 ,9, 11
import express from 'express'
import authCtrl from '../controllers/auth.controller'

const router = express.Router()

//POST request to authenticate the user with their email and password
//GET request to clear the cookie containing a JWT
// that was set on the response object after sign-in
//POST req and GET req signout routes will invoke the controller functions
//defined in the auth.controller.js file
router.route('/auth/signin')
  .post(authCtrl.signin)//exe the signin controller function in server/controllers/auth.controllers.js
router.route('/auth/signout')
  .get(authCtrl.signout)//exe the signout controller function in server/controllers/auth.controllers.js

export default router
