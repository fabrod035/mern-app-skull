import User from '../models/user.model'
import jwt from 'jsonwebtoken'
import expressJwt from 'express-jwt'
import config from './../../config/config'

//envoke by the signin route router/auth.routes.js
//post req object receives the email and password in req.body
//This email is used to retrive a matching user from the db
//Then, the password authentication method defined in UserSchema
// is used to verify the password that's received in req.body from the client
//If the password is successfully verified
// the JWT module is used to generate a signed JWT 
//using a secret key and the user's _id value
const signin = async (req, res) => {
  try {
    let user = await User.findOne({
      "email": req.body.email
    })
    if (!user)
      return res.status('401').json({
        error: "User not found"
      })

    if (!user.authenticate(req.body.password)) {
      return res.status('401').send({
        error: "Email and password don't match."
      })
    }

    const token = jwt.sign({
      _id: user._id
    }, config.jwtSecret)

    res.cookie("t", token, {
      expire: new Date() + 9999
    })

    return res.json({
      token,
      user: {
        _id: user._id,
        name: user.name,
        email: user.email
      }
    })

  } catch (err) {

    return res.status('401').json({
      error: "Could not sign in"
    })

  }
}


//envoke by the signout route router/auth.routes.js
//signout function clears the response cookie containing the signed JWT.
const signout = (req, res) => {
  res.clearCookie("t")
  return res.status('200').json({
    message: "signed out"
  })
}


//checks for authentication
//requireSignin method in auth.controller.js 
//uses express-jwt to verify that the incoming request has a valid JWT 
//in the Authorization header. 
//If the token is valid, it appends the verified user's ID 
//in an 'auth' key to the request object; otherwise,
// it throws an authentication error
//We can add requireSignin to any route 
//that should be protected against unauthenticated access
const requireSignin = expressJwt({
  secret: config.jwtSecret,
  userProperty: 'auth'
})


//checks for authorization
//on top of checking authentication we also want ot me sure
//the requesting user is only updating or deleting their own user info
//will check whether the authenticated user is the same as the user 
//being updated or deleted before the corresponding CRUD controller function is allowed to proceed

//req.auth object is populated by express-jwt 
//in requireSignin after authentication verification, 
//while req.profile is populated by the userByID function in
// user.controller.js. We will add the hasAuthorization function
// to routes that require both authentication and authorization
const hasAuthorization = (req, res, next) => {
  const authorized = req.profile && req.auth && req.profile._id == req.auth._id
  if (!(authorized)) {
    return res.status('403').json({
      error: "User is not authorized"
    })
  }
  next()
}

export default {
  signin,
  signout,
  requireSignin,
  hasAuthorization
}
