import mongoose from 'mongoose'
import crypto from 'crypto'
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: 'Name is required'
  },
  about:{
    type: String,
    trim: true
  },
  email: {
    type: String,
    trim: true,
    unique: 'Email already exists',
    match: [/.+\@.+\..+/, 'Please fill a valid email address'],
    required: 'Email is required'
  },
  hashed_password: {
    type: String,
    required: "Password is required"
  },
  salt: String,
  updated: Date,
  created: {
    type: Date,
    default: Date.now
  },
  photo: {
    data: Buffer,
    contentType: String
  },
})

//An image file that's uploaded by the user from the client- side 
//will be converted into binary data and stored in this photo field 
//for documents in the Users collection in MongoDB

//user creation or update password, 
//it is encrypted into a new hashed value and 
//set to the hashed_password field, 
//along with the unique salt value in the salt field
UserSchema
  .virtual('password')
  .set(function(password) {
    this._password = password
    this.salt = this.makeSalt()
    this.hashed_password = this.encryptPassword(password)
  })
  .get(function() {
    return this._password
  })
//logic for password
UserSchema.path('hashed_password').validate(function(v) {
  if (this._password && this._password.length < 6) {
    this.invalidate('password', 'Password must be at least 6 characters.')
  }
  if (this.isNew && !this._password) {
    this.invalidate('password', 'Password is required')
  }
}, null)


//The encryption logic and salt generation logic, 
//which are used to generate the hashed_password and salt values
//representing the password value, are defined as UserSchema methods
UserSchema.methods = {
  authenticate: function(plainText) {//verify sign-in attemps
    return this.encryptPassword(plainText) === this.hashed_password
  },
  //generate an encrypted hash
  encryptPassword: function(password) {
    if (!password) return ''
    try {
      return crypto
        .createHmac('sha1', this.salt)
        .update(password)
        .digest('hex')
    } catch (err) {
      return ''
    }
  },
  //generates a unique and random salt value
  makeSalt: function() {
    return Math.round((new Date().valueOf() * Math.random())) + ''
  }
}

export default mongoose.model('User', UserSchema)
